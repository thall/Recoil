function TabContext(fileEntry) {
    var self = this;
    var tab;
    var frame;
    var file = fileEntry;
    var name = fileEntry.name;
    var xml;
    var xmlSerializer = new XMLSerializer();
    
    tab = $(document.createElement('li'));
    tab.attr('role', 'presentation');
    var link = '';
    link = link.concat('<a href="#">', name, '</a>');
    tab.html(link);
    
    $('a', tab).click(function() {
        tabManager.activateTab(self);
    });
    
    frame = $(document.createElement('div'));
    frame.addClass('row');
    
    var tree = $(document.createElement('div'));
    tree.addClass('col-md-3');
    tree.attr('role', 'tree');
    
    var editor = $(document.createElement('div'));
    editor.addClass('col-md-9');
    editor.attr('role', 'editor');
    
    var item1 = $(document.createElement('div'));
    item1.addClass('large-item');
    var item2 = $(document.createElement('div'));
    item2.addClass('large-item');
    
    tree.append(item1);
    editor.append(item2);
    
    frame.append(tree, editor);
    
    this.activate = function(ctx) {
        if(!tab || !frame) {
            return false;
        }
        
        $('li', ctx.tabs).removeClass('active');
        tab.addClass('active');
        
        $('div', ctx.wrapper).removeClass('active');
        frame.addClass('active');
        
        return true;
    };
    this.setFile = function(newFile) {
        file = newFile;
        name = newFile.name;
        $('a', tab).text(name);
    };
    
    this.getTab = function() {
        return tab;
    };
    this.getFrame = function() {
        return frame;
    };
    this.getName = function() {
        return name;
    }
    
    this.setXML = function(data) {
        xml = data;
        // TODO: Load tab
        return true;
    };
    this.getXML = function() {
        var data = xmlSerializer.serializeToString(xml);
        return data;
    };
    
    this.writeToFile = function() {
        var xml = this.getXML();
        try {
            file.createWriter(function(writer) {
                try {
                    var tag = $('a', tab);
                    
                    writer.onwriteend = function() {
                        tag.text(name);
                        console.log('saved to file ' + name);
                    };
                    
                    var data = new Blob([xml], { type: 'application/xml' });
                    writer.write(data);
                    
                } catch(err) {
                    console.log(err.message);
                }
            });
        } catch(err) {
            console.log(err.message);
            return false;
        }
        return true;
    };
    
    this.destroy = function() {
        tab.remove();
        frame.remove();
        // TODO: Exit tab
    };
}
