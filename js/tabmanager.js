function TabManager(context) {
    var ctx  = context;
    var tabs = Array();
    var activeIndex = -1;
    
    this.addTab = function(tab) {
        activeIndex = tabs.length;
        tabs.push(tab);
        
        ctx.tabs.append(tab.getTab());
        ctx.wrapper.append(tab.getFrame());
        
        tab.activate(ctx);
    };
    this.removeTab = function(index) {
        var nextIndex;
        if(index == activeIndex) {
            if(tabs.length > 1) {
                if(index != 0) {
                    nextIndex = index - 1;
                    activeIndex = nextIndex;
                } else {
                    nextIndex = 1;
                    activeIndex = 0;
                }
                
                tabs[nextIndex].activate(ctx);
            } else {
                activeIndex = -1;
            }
        }
        
        var tab = tabs[index];
        
        tabs.splice(index, 1);
        
        if(tab) {
            tab.destroy();
        }
    };
    this.activateTab = function(tab) {
        var index = tabs.indexOf(tab);
        if(activeIndex == index) {
            return true;
        } else {
            if(index != -1 && tab.activate(ctx)) {
                activeIndex = index;
                return true;
            } else {
                return false;
            }
        }
    };
    this.activateIndex = function(index) {
        if(activeIndex == index) {
            return true;
        } else {
            if((index > -1 && index < tabs.length)
              && tabs[index].activate(ctx)) {
                activeIndex = index;
                return true;
            } else {
                return false;
            }
        }
    };
    this.getActiveIndex = function() {
        return activeIndex;
    };
    this.getActive = function() {
        if(activeIndex > -1 && activeIndex < tabs.length) {
            return tabs[activeIndex];
        } else {
            return null;
        }
    };
    
    this.getTabs = function() {
        return tabs;
    };
    this.tabCount = function() {
        return tabs.length;
    };
}
