var tabManager;

var xmlDoc = '<?xml version="1.0" encoding="UTF-8"?>\n<document>\n    <introduction>\n    </introduction>\n    <description>\n    </description>\n    <requirements>\n    </requirements>\n</document>'

function startNewTab(fileEntry) {
    var tab = new TabContext(fileEntry);
    
    var xml = $.parseXML(xmlDoc);
    
    if(tab.setXML(xml) && tab.writeToFile()) {
        tabManager.addTab(tab);
        return true;
    } else {
        tab.destory();
        return false;
    }
}

function openNewTab(fileEntry) {
    var tab = new TabContext(fileEntry);
    
    fileEntry.file(function(file) {
        var reader = new FileReader();
        
        reader.onloadend = function(e) {
            var xml = $.parseXML(e.target.result);
            tab.setXML(xml);
        };
        
        reader.readAsText(file);
    });
    
    tabManager.addTab(tab);
}

var fileTypes = [{
    description: 'Recoil Software Requirements Document',
    mimeTypes: ['application/xml'],
    extensions: ['rsrd']
}]

function initFileListeners() {
    $('#newFile').click(function() {
        chrome.fileSystem.chooseEntry({
            type: 'saveFile',
            accepts: fileTypes
        },
        function(file) {
            if(chrome.runtime.lastError) {
                console.log(chrome.runtime.lastError);
            } else {
                startNewTab(file);
            }
        });
        
        return true;
    });
    
    $('#openFile').click(function() {
        chrome.fileSystem.chooseEntry({
            type: 'openFile',
            accepts: fileTypes,
            acceptsMultiple: false
        },
        function(fileEntries) {
            if(chrome.runtime.lastError) {
                console.log(chrome.runtime.lastError);
            } else {
                $.each(fileEntries, function(index, file) {
                    openNewTab(file);
                });
            }
        });
        
        return true;
    });
    
    $('#saveFile').click(function() {
        tabManager.getActive().writeToFile();
        return true;
    });
    
    $('#saveAsFile').click(function() {
        chrome.fileSystem.chooseEntry({
            type: 'saveFile',
            suggestedName: tabManager.getActive().getName(),
            accepts: fileTypes
        },
        function(file) {
            if(chrome.runtime.lastError) {
                console.log(chrome.runtime.lastError);
            } else {
                var active = tabManager.getActive()
                active.setFile(file);
                active.writeToFile();
            }
        });
        return true;
    });
    
    $('#closeFile').click(function() {
        tabManager.removeTab(tabManager.getActiveIndex());
    });
}

function initEditListeners() {
    $('#createChild').click(function() {
        
        return true;
    });
    
    $('#cutReq').click(function() {
        
        return true;
    });
    
    $('#copyReq').click(function() {
        
        return true;
    });
    
    $('#pasteReq').click(function() {
        
        return true;
    });
}

function initDocumentsListeners() {
    $('#saveAllDocs').click(function() {
        $.each(tabManager.getTabs(), function(index, tab) {
            tab.writeToFile();
        });
        return true;
    });
    
    $('#closeAllDocs').click(function() {
        while(tabManager.tabCount() > 0) {
            tabManager.removeTab(0);
        }
        return true;
    });
    
    $('#generateDoc').click(function() {
        console.log('Not Implemented');
        return false;
    });
}

$(function() {
    tabManager = new TabManager({
        tabs: $('#editorTabs'),
        wrapper: $('#editorWrapper'),
    });
    
    initFileListeners();    
    initEditListeners();
    initDocumentsListeners();
});
